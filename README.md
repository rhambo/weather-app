# WeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6. and upgraded to 12.2.1 , it shows a live weather data of five selected cities thanks to the openweather api (https://openweathermap.org/)
## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).



## Architecture

The application has three components , with the app component serving as the base components where most of the actions are performed, the others are

-- Weather Card -- Accepts the weather data of a city as an input and has an click listener attached to it to display the forecast for the next 4 hours

-- Weather Details -- Displays as a side bar , it emits an event that lets the the parent component know when to hide/close it, it displays the forecast of the next four hours of the selected city


NOTE: The two components are majorly dumb components are used for rendering purposes majorly

The OnPush Change Detection Strategy was used for those components as way to reduce the no of time the change detection tree is transversed through


## Pipes
There are two pipes in use

-- ToCelcius -- Althoght the api allows us to pass in the unit we'd like the temperature to be returned in , to add a bit more functionality to the app , i wrote a custom pipe that takes in the default temperature in Kelvin and returns it in Celcius.

-- EpochToLocal -- It converts the Epoch timestamp to a readable date/time


-- ImageURl -- renders a sanitized url to the image src attribute

## Service
There's only one major service which is the weather service , it uses a series of RxJs operators to perform different operations
Worthy Of note is the fetchWeathers method which helps make the call to the endpoint to get the weather of each city emitted for the city array via the 'from' operator and returns the data sequentially via the concatMap operator in an Observable array

its worthy of note because the api allows us input certain long and lat of a region to get the weather for a group of cities but since want to be able to identify the weather of cities in different regions i chose this approach

## CSS
No framework was used or needed


