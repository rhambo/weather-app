import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {of} from 'rxjs';
import { ISingleCityWeather } from './models/weather.model';
import { WeatherService } from './services/weather.service';
describe('AppComponent', () => {

let fixture: any;
let app: any;
let MockService: any;

  let mockSingleWeatherData: ISingleCityWeather[] = [{
      "coord": {
        "lon": 444.43,
        "lat": 444.43
      },
      "weather": [
        {
          "id": 444.43,
          "main": 'rer',
          "description": 'rer',
          "icon": 'rer'
        }
      ],
      "base": 'rer',
      "main": {
        "temp": 444.43,
        "feels_like": 444.43,
        "temp_min": 444.43,
        "temp_max": 444.43,
        "pressure": 444.43,
        "humidity": 444.43
      },
      "visibility": 444.43,
      "wind": {
        "speed": 444.43,
        "deg": 444.43
      },
      "clouds": {
        "all": 444.43
      },
      "dt": 444.43,
      "sys": {
        "type": 444.43,
        "id": 444.43,
        "message": 444.43,
        "country": 'rer',
        "sunrise": 444.43,
        "sunset": 444.43
      },
      "timezone": 444.43,
      "id": 444.43,
      "name": 'rer',
      "cod": 444.43
  }];
  beforeEach(async () => {
    MockService = jasmine.createSpyObj(['getHourlyWeatherInfo', 'getWeatherData'])

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [{
        provide: WeatherService,
        useValue: MockService
      }]
    }).compileComponents();

     fixture = TestBed.createComponent(AppComponent);
     app = fixture.componentInstance;
     

  });


  it('should call getHourlyWeatherInfo service method', () => {

    app.cities$ = of(mockSingleWeatherData);
    fixture.detectChanges();
    console.log(fixture.debugElement.nativeElement)
    const weatherCard = fixture.debugElement.query(By.css('app-weather-card'));
  
    weatherCard.nativeElement.click();
    expect(MockService.getHourlyWeatherInfo).toHaveBeenCalled();
  });

  
  it('should remove details from DOM', () => {
    fixture.detectChanges();

    app.cities$ = of(mockSingleWeatherData);
    fixture.detectChanges();
    console.log(fixture.debugElement.nativeElement)
    const weatherCard = fixture.debugElement.query(By.css('app-weather-card'));
  
    weatherCard.nativeElement.click();
    fixture.detectChanges();

    console.log('Before' , fixture.nativeElement);

    app.closeDetails();

    fixture.detectChanges();



    console.log('After', fixture.nativeElement);


    
    // expect(MockService.getHourlyWeatherInfo).toHaveBeenCalled();
  });
});
