import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { ISingleCityWeather } from 'src/app/models/weather.model';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherCardComponent implements OnInit {
@Input() weatherData!: ISingleCityWeather;
  

  constructor() { }

  ngOnInit(): void {
  }

}
