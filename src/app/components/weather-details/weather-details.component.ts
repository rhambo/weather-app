import { trigger, transition, style, animate } from '@angular/animations';
import { EventEmitter, OnChanges, Output, SimpleChanges } from '@angular/core';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IHourlyWeatherData } from 'src/app/models/hourlyWeatherData.model';


@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('500ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('500ms ease-in', style({transform: 'translateX(80%)'}))
      ])
    ])
  ]
})
export class WeatherDetailsComponent implements OnInit, OnChanges {

  @Input() hourlyWeatherReport!: IHourlyWeatherData;

  @Output() onCloseClick = new EventEmitter(false);


  constructor() { }

  ngOnInit(): void {

    
  }

  ngOnChanges(changes?: SimpleChanges) {
    
    for (let property in changes) {
      if (property === 'hourlyWeatherReport') {
        this.hourlyWeatherReport = changes[property].currentValue;
        this.hourlyWeatherReport.hourly = this.hourlyWeatherReport.hourly.slice(0, 4)
        
      } 
  }
  }


  closeDetails() {    
    this.onCloseClick.emit(false);
  }

}