import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherService } from './services/weather.service';
import { ISingleCityWeather } from './models/weather.model';
import { IHourlyWeatherData } from './models/hourlyWeatherData.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent {
  cities$!: Observable<ISingleCityWeather[]>;
  toggleDetailsView: boolean = false;
  hourlyWeatherReport!: IHourlyWeatherData;




  constructor(private weatherService: WeatherService) {
      this.cities$ = this.weatherService.getWeatherData();
  }



  ngOnInit() {
  }




  getHourlyWeatherInfo(city: ISingleCityWeather) {    
    this.weatherService.getHourlyWeatherInfo(city).subscribe((res)=> {
      this.hourlyWeatherReport = res;
      this.toggleDetailsView = true;

    })
  }

  closeDetails(val: boolean) {
    this.toggleDetailsView = val;
  }


 
}
