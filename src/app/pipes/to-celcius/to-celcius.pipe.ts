import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toCelcius'
})
export class ToCelciusPipe implements PipeTransform {

  transform(tempInKelvin: number): number {
    return Number((tempInKelvin - 273.15).toFixed(2));

  }

}
