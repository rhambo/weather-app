import { ToCelciusPipe } from './to-celcius.pipe';

describe('ToCelciusPipe', () => {
  let pipe: ToCelciusPipe;

  beforeEach(() => {
    pipe = new ToCelciusPipe();
  });

  it('create an instance', () => {
    const pipe = new ToCelciusPipe();
    expect(pipe).toBeTruthy();
  });

  it('should convert temperature to Celcius', () => {
   let tempInK = 523;
      expect(pipe.transform(tempInK)).toEqual(249.85);
  });
});
