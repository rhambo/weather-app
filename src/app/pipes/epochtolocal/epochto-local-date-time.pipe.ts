import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'epochtoLocalDateTime'
})
export class EpochtoLocalDateTImePipe implements PipeTransform {

  transform(epoch: number): string {


    return new Date(epoch * 1000).toLocaleString();
  }

}
