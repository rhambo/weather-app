import { EpochtoLocalDateTImePipe } from './epochto-local-date-time.pipe';

describe('EpochtoLocalDateTImePipe', () => {
  it('create an instance', () => {
    const pipe = new EpochtoLocalDateTImePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return epoch timestamp to LocalDateString', () => {
    const pipe = new EpochtoLocalDateTImePipe();
    let expectedResult = '8/12/2021, 1:00:00 PM';

    expect(pipe.transform(1628769600)).toEqual(expectedResult)
  });
});
