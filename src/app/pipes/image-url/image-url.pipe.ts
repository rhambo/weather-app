import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'imageUrl'
})
export class ImageUrlPipe implements PipeTransform {

  constructor(private _sanitizer: DomSanitizer) {

  }

  transform(iconcode: string): SafeResourceUrl {
    console.log(this._sanitizer.bypassSecurityTrustResourceUrl(`http://openweathermap.org/img/w/${iconcode}.png`));
    return this._sanitizer.bypassSecurityTrustResourceUrl(`http://openweathermap.org/img/w/${iconcode}.png`);
  }

}
