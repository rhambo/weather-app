import { inject } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageUrlPipe } from './image-url.pipe';

describe('ImageUrlPipe', () => {


  it('create an instance', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
    let pipe = new ImageUrlPipe(sanitizer);
    expect(pipe).toBeTruthy();
  })); 


  it('should be a safe URL', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
        let url = 'http://openweathermap.org/img/w/10n.png';
        let sanitizedUrl = sanitizer.bypassSecurityTrustResourceUrl(url);
        console.log(typeof url)
       expect(sanitizedUrl).toEqual(jasmine.objectContaining({changingThisBreaksApplicationSecurity: url}));
   }));

  
});
