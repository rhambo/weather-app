import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, Observable, Subject, timer } from 'rxjs';
import { switchMap, retry,takeUntil, toArray , map, tap, concatMap} from 'rxjs/operators';
import { ISingleCityWeather } from '../models/weather.model';
import { IHourlyWeatherData } from '../models/hourlyWeatherData.model';


const KEY = '3642b4b4291fd665fa7fee800548a7fd';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
private endSubs$ = new Subject();

public cities$!: Observable<ISingleCityWeather[]>;

  constructor(private httpClient: HttpClient) {
   }


  public getWeatherData(): Observable<ISingleCityWeather[]> {
    
    this.cities$ = timer(1,1000000).pipe(switchMap(()=> this.fetchWeathers()), retry(),
    takeUntil(this.endSubs$));
     return this.cities$;
  
  } 


  private fetchWeathers(): Observable<ISingleCityWeather[]> {
    const city = ['London', 'Berlin', 'New York', 'Lagos', 'Amsterdam'];

    return from(city).pipe(
     concatMap(city=> this.httpClient.get<ISingleCityWeather>(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${KEY}`))
    ,toArray())

  }


  public getHourlyWeatherInfo(city: ISingleCityWeather): Observable<IHourlyWeatherData> {    
    return this.httpClient.get<IHourlyWeatherData>(`https://api.openweathermap.org/data/2.5/onecall?lat=${city.coord.lat}&lon=${city.coord.lon}&exclude=minutely,daily&appid=${KEY}`)
  }

  ngOnDestroy() {
    this.endSubs$.next();
    this.endSubs$.complete();
  }
}
