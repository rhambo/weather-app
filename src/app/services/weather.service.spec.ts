
import { ISingleCityWeather } from '../models/weather.model';
import { WeatherService } from './weather.service';

describe('WeatherService', () => {
  let service = jasmine.createSpyObj(['getWeatherData', 'getHourlyWeatherInfo']); //allows the test service have access to methods in service since we arent testing HTTP calls and wont be using TestBed to inject them
   
  let mockSingleWeatherData: ISingleCityWeather = {
    "coord": {
      "lon": -0.13,
      "lat": 51.51
    },
    "weather": [
      {
        "id": 300,
        "main": "Drizzle",
        "description": "light intensity drizzle",
        "icon": "09d"
      }
    ],
    "base": "stations",
    "main": {
      "temp": 280.32,
      "pressure": 1012,
      "humidity": 81,
      "temp_min": 279.15,
      "temp_max": 281.15,
      "feels_like": 444.4
    },
    "visibility": 10000,
    "wind": {
      "speed": 4.1,
      "deg": 80
    },
    "clouds": {
      "all": 90
    },
    "dt": 1485789600,
    "sys": {
      "type": 1,
      "id": 5091,
      "message": 0.0103,
      "country": "GB",
      "sunrise": 1485762037,
      "sunset": 1485794875
    },
    "timezone": 4444,
    "id": 2643743,
    "name": "London",
    "cod": 200
    }
                

  

  beforeEach(() => {


  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a SingleCityWeather data', () => {
    service.getWeatherData.and.returnValue(mockSingleWeatherData); // Making sure that the response from getWeath returns the mock data
    let response = service.getWeatherData();
    expect(response).toEqual(mockSingleWeatherData);
  });
});
