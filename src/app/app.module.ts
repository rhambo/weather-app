import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToCelciusPipe } from './pipes/to-celcius/to-celcius.pipe';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { WeatherDetailsComponent } from './components/weather-details/weather-details.component';
import { EpochtoLocalDateTImePipe } from './pipes/epochtolocal/epochto-local-date-time.pipe';
import { ImageUrlPipe } from './pipes/image-url/image-url.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ToCelciusPipe,
    WeatherCardComponent,
    WeatherDetailsComponent,
    EpochtoLocalDateTImePipe,
    ImageUrlPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
